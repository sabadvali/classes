
# მითითებული კლასი იქმნება mock ისთვის რადგან მოვახდინოთ ჯერ არარსებული 
# ბაზის სიმულაცია. 
class itemdatabase:
    def __init__(self) -> None:
        pass

        # მითითებული ფუნქცია იღებს სტრინგს და აბურნებს ფლოატს.
    def get_data(self, item:str) -> float:
        pass

