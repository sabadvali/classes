from class_for_unit import shoppingcart
from item_data_base import itemdatabase
# მითითებული ბიბლიოთეკა გვჭირდება რომ არარსებული ბაზის ან ფუნქციის 
# სიმულაციაში გვეხმარება რათა არ შეგვაფერხოს კოდის წერაში ჯერ არარსებულმა ფუნქცებმა ბაზებმა და ასე შემდეგ.
from unittest.mock import Mock
# მითითებული ბიბლიოთეკა გვეხმარება უნიტტესტების დაწერაში.
import pytest


# ამ შემთხვევაში დეკორატორს ვიყენებთ რადგან დუბლირებული არგვქონდეს კოდი
# ყოველ ფუნქციაში გვიწევდა კლასის ინიციალიზაცია. და ობიექტის მინიჭება.
@pytest.fixture
def cart():
     return shoppingcart(5)

# ყველა ფუმქციაში უნდა იყოს ჰგამოყენებული test სახელი.
# რადგან გაეშვას უნით ტესტი
def test_can_add_item_to_cart(cart):
    # cart = shoppingcart(5)

    cart.add("apple")
    # ასერტი გვეხმარება უნიტ ტესტებში.
    # კლასის მეთოდი size მა უნდა დააბრუნოს აუცილებად 1
    # როდესაც add ის საშვალებით გადავცემთ ერთ ობიექტს.
    # თუ დაბრუნებული მნიშვნელობა იქნება ერთი.ტესტი წარმატებით გაივლის.
    assert cart.size() == 1


def test_when_item_added_then_cart_contains_items(cart):
    # cart = shoppingcart(5)
    
    cart.add("apple")
    # მარტივად ვამოჭმებთ მითითებული ფუნქცია დააბრუნებს თუ არა 
    # apple ს.
    assert "apple" in cart.get_item()

def test_when_add_more_than_max_item(cart):
    for i in range(5):
        cart.add("apple")
        # კონკრეტული ოვერფლოუს დაჭერა ხდება
    with pytest.raises(OverflowError):
        cart.add("banana")


def test_can_get_total_price(cart):
        # cart = shoppingcart(5)
        cart.add("apple")
        cart.add("orange")

        item_database = itemdatabase()

        def mock_get_item(item: str):
             if item == "apple":
                  return 1.7
             if item == "orange":
                  return 2.7
        # side_effect ის საშვალებით ვიძახებთ ფუნქციას სადაც განსაზღვრულია ფასები
        # price_map ი მაგივრად ვიყენებთ sside_effect
        item_database.get_data = Mock(side_effect=mock_get_item)
        # მოკის საშვალებით არასებული ბაზიდან ვაბრუნებინებთ მონაცემებს 
        # გამოიყენება იმისთვის რომ მოვახერხოთ კოდის წერა ისე რომ ბაზები ჯერ შექმნილიც კი არიყოს.
        # price_map ამოღებული მნაცემის mock ს ვაკეთებთ.
        # ქვემოთ მოცემული assert უკვე ამათ ჯამს აბრუნებს.
        # item_database.get_data = Mock(return_value= 2.10)

        # price_map = {
        #     "apple": 1.5,
        #     "orange": 2.7
        # }

        # cart.get_total_price(price_map)
        # მიღებული მონაცემი უნდა დემთხვეს მითითებულ ციფრებს.
        assert cart.get_total_price(item_database) == 4.40

