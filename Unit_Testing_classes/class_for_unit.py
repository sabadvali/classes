from typing import List



class shoppingcart:
    # შემოსული მონაცემი იქნება int, max_size ში.
    # none ნიშნავს რომ მიტითებული მეტოდი ან ფუნქცია არ აბრუნებს არაფერს.
    # ეს ყველაფერი გამოიყენება დოკუმენტაციისთვის, კარგი სტილის და კოდის გარჩევადობის მიზნით
    def __init__(self, max_size: int) -> None:
        # ეთითება რომ ის აუცილებლად იქნება სტრინგი და ლისტი.
        self.items: List[str] = []
        self.max_size = max_size
    
    def add(self, item: str):
        if self.size() == self.max_size:
            # ჩაშენებული მოდული raise ვაკეთებ შენიშვვნას.
            raise OverflowError("cannot add more items")

        self.items.append(item)

        

    def size(self) -> int:
        return len(self.items)
    # მიტითებული ფუნქცია აბრუნებს ლისტს რაც მითითებულია. ამ ლისტში არის სტრინგი ასევე.
    def get_item(self) -> List[str]:
        return self.items


    def get_total_price(self,price_map):
        count = 0.0
        for i in self.items:
            # შეგვიძლია ესე ამოვიღოთ value 
            count = count + price_map.get_data(i)
            # ესეც შეიძლება
            # count = count + price_map[i]
        return count
        