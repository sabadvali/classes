

class shoping_cart:

    def  __init__(self):
        self.list_of_items= []
        self.max_number_of_items = 0

    def define_max_number_of_items(self):
        print("how many item you plan to buy")
        self.max_number_of_items = int(input())
    
    def add_item(self, item_name):

        if (len(item_name) > self.max_number_of_items):
            print("basket is full")
        else: 
            self.list_of_items.append(item_name)
            self.get_total_price()
    
    def get_what_i_bought(self):
        if self.list_of_items:
            print("you bought this items:",self.list_of_items[0])


    def pay(self, total_sum):
        answer = str(input("want buy?: yes/no: "))
        if answer == "yes":
            money = float(input("please pay money: "))
            if money > total_sum:
                money = money - total_sum
                print("here is your exchange:",money, "GEL")
            elif money < total_sum:
                total_sum = total_sum - money
                print("please add more money:", total_sum, "GEL")
            elif money == total_sum:
                print("Congratulations")
                self.get_what_i_bought()
        else:
            print("bye")
            
    def get_total_price(self):
        items_we_dont_have = []
    
        price = {
                    "apple": 1.5,
                    "banana": 1.8,
                    "pear": 2.5,
                    "grape": 5.6,
                    "orange": 4.5
                }
        total_sum = 0.0

        if self.list_of_items:
            for item in self.list_of_items[0]:
                if item in price:
                    total_sum = total_sum + price[item]
                else:
                   items_we_dont_have.append(item)     
   
        if items_we_dont_have: 
            print("sorry we dont have this item yet:", items_we_dont_have)
            for i in items_we_dont_have:
                self.list_of_items[0].remove(i)

        print("your items total price is: ",total_sum, "GEL")
        self.pay(total_sum)

         